import torch
import numpy as np
import json
from torchvision import datasets, transforms, models
from torch import nn, optim
from torch.utils.data.sampler import SubsetRandomSampler
from torch.optim import lr_scheduler
from collections import OrderedDict
from utils import *
import sys, getopt


class Model:

    def __init__(self, batch_size = 64):

        self.resources = {}

        with open('cat_to_name.json', 'r') as f:
            self.resources['cat_to_name'] = json.load(f)

        self.resources['batch_size'] = batch_size

        self.resources['train_cost'] = np.array([])
        self.resources['valid_cost'] = np.array([])

        self.resources['valid_size'] = 0.2

        self.resources['test_size'] = 0.2

        self.create_model()


    def create_model(self):
        # load a pre-trained model
        self.resources['model'] = models.resnet50(pretrained=True)

        # create new feedforward layers
        classifier = nn.Sequential(OrderedDict([
            ('fc1', nn.Linear(2048, 1000)),
            ('relu', nn.ReLU()),
            ('dropout', nn.Dropout(0.6)),
            ('fc2', nn.Linear(1000, 102)),
            ('softmax', nn.LogSoftmax(dim=1))
        ]))
        self.resources['model'].fc = classifier

        # initialize current epoch value
        self.resources['current_epoch'] = 0

        # specify cost function
        self.resources['criterion'] = nn.NLLLoss()

        # specify optimizer
        self.resources['optimizer'] = optim.SGD(self.resources['model'].parameters(), lr=0.01)

        # specify learning rate decay
        self.resources['scheduler'] = lr_scheduler.StepLR(self.resources['optimizer'], step_size=9, gamma=0.9)

        # set the starting epoch number
        self.resources['best_epoch'] = 0

        # set the minimum validation lost value
        self.resources['valid_cost_min'] = np.Inf

        self.resources['best_model_state_dict'] = self.resources['model'].state_dict()

        self.resources['valid_idx'], self.resources['test_idx'], self.resources['train_idx'] = None, None, None


    def load(self, images_path, labels_path, new=True):


        try:
            images = np.array([images_path+'/'+file for file in os.listdir(images_path)])
        except:
            sys.exit("Dataset is missing! Please add the dataset to 'dataset/images/' folder.")

        try:
            labels = np.array(scipy.io.loadmat(labels_path)['labels'][0])
        except:
            sys.exit("Labels are missing! Please add the 'imagelabels.mat' file to 'dataset/annotations/' folder.")

        if new:
            # store the class_to_index mapping
            self.resources['class_to_idx'] = {list(set(labels))[i]: i for i in range(len(set(labels)))}

            # calculate idx_to_class mapping, reverse of class_to_idx mapping
            self.resources['idx_to_class'] = {val: key for key, val in self.resources['class_to_idx'].items()}

        # map the labels to a distribution of consecutive values, starting with 0
        compact_labels = np.array([self.resources['class_to_idx'][i] for i in labels])

        # if new, load the previous distribution (will allow  to continue training and perform testing)
        if new:
            # select shuffled indices for different sets
            self.resources['train_idx'], self.resources['valid_idx'], self.resources['test_idx'] = splitData(
                compact_labels, self.resources['valid_size'], self.resources['test_size'])

        train_images = images[self.resources['train_idx']].tolist()
        valid_images = images[self.resources['valid_idx']].tolist()
        test_images = images[self.resources['test_idx']].tolist()

        train_labels = compact_labels[self.resources['train_idx']].tolist()
        valid_labels = compact_labels[self.resources['valid_idx']].tolist()
        test_labels = compact_labels[self.resources['test_idx']].tolist()

        # define transforms for the training, validation, and testing sets
        train_transforms = transforms.Compose([transforms.RandomRotation(45),
                                               transforms.RandomResizedCrop(224, scale=(0.6, 1.0)),
                                               transforms.CenterCrop(224),
                                               transforms.RandomHorizontalFlip(),
                                               transforms.ToTensor(),
                                               transforms.Normalize([0.485, 0.456, 0.406],
                                                                    [0.229, 0.224, 0.225])])

        valid_test_transforms = transforms.Compose([transforms.Resize(224),
                                                    transforms.CenterCrop(224),
                                                    transforms.ToTensor(),
                                                    transforms.Normalize([0.485, 0.456, 0.406],
                                                                         [0.229, 0.224, 0.225])])

        # load the datasets
        train_dataset = CustomDataset(train_images, train_labels, transform=train_transforms)
        valid_dataset = CustomDataset(valid_images, valid_labels, transform=valid_test_transforms)
        test_dataset = CustomDataset(test_images, test_labels, transform=valid_test_transforms)

        # define the dataloaders
        self.resources['train_dataloader'] = torch.utils.data.DataLoader(train_dataset, batch_size=self.resources['batch_size'], shuffle=True)
        self.resources['valid_dataloader'] = torch.utils.data.DataLoader(valid_dataset, batch_size=self.resources['batch_size'], shuffle=True)
        self.resources['test_dataloader'] = torch.utils.data.DataLoader(test_dataset, batch_size=self.resources['batch_size'], shuffle=True)




    def train(self, n_epochs):
        # move model to GPU is CUDA if available
        if torch.cuda.is_available():
            self.resources['model'].cuda()

        for epoch in range(self.resources['current_epoch'], self.resources['current_epoch'] + n_epochs):

            # keep track of training and validation cost
            train_cost = 0.0
            valid_cost = 0.0

            # add a step to the learning rate decat
            self.resources['scheduler'].step()

            # train the model
            self.resources['model'].train()

            for batch_idx, (data, target) in enumerate(self.resources['train_dataloader']):

                # move tensors to GPU if CUDA is available
                if torch.cuda.is_available():
                    data, target = data.cuda(), target.cuda()

                # clear the gradients of all optimized variables
                self.resources['optimizer'].zero_grad()

                # forward pass
                output = self.resources['model'](data)

                # calculate the batch cost
                cost = self.resources['criterion'](output, target)

                # backward pass: compute gradient of the cost with respect to model parameters
                cost.backward()

                # perform a single optimization step (parameter update)
                self.resources['optimizer'].step()

                # update training cost
                train_cost += cost.item() * data.size(0)


            # calculate average training cost
            train_cost = train_cost / len(self.resources['train_dataloader'].dataset)

            self.resources['train_cost'] = np.append(self.resources['train_cost'], train_cost)

            # validate the model
            self.resources['model'].eval()

            class_correct = list(0. for i in range(len(self.resources['test_dataloader'].dataset.classes)))
            class_total = list(0. for i in range(len(self.resources['test_dataloader'].dataset.classes)))

            # disable gradients
            with torch.no_grad():
                for batch_idx, (data, target) in enumerate(self.resources['valid_dataloader']):

                    # move tensors to GPU if CUDA is available
                    if torch.cuda.is_available():
                        data, target = data.cuda(), target.cuda()

                    # forward pass
                    output = self.resources['model'](data)

                    # convert output probabilities to predicted class
                    _, pred = torch.max(output, 1)

                    # compare predictions to true label
                    correct_tensor = pred.eq(target.data.view_as(pred))

                    # use appropriate dimensions
                    if torch.cuda.is_available():
                        correct = np.squeeze(correct_tensor.cpu().numpy())
                    else:
                        correct = np.squeeze(correct_tensor.numpy())

                    # calculate test accuracy for each object class
                    for i in range(len(data.data)):
                        label = target.data[i]-1
                        class_correct[label] += correct[i].item()
                        class_total[label] += 1

                    # calculate the batch cost
                    cost = self.resources['criterion'](output, target)

                    # update average validation cost
                    valid_cost += cost.item() * data.size(0)


                # calculate average validation cost
                valid_cost = valid_cost / len(self.resources['valid_dataloader'].dataset)

                # store the cost for later reference
                self.resources['valid_cost'] = np.append(self.resources['valid_cost'], valid_cost)

                # print training/validation statistics
                print('Epoch: {} \tTraining Cost: {:.6f} \tValidation Cost: {:.6f}'.format(
                    epoch, train_cost, valid_cost))

                # keep model if validation cost has decreased
                if valid_cost <= self.resources['valid_cost_min']:
                    # print validation cost devrease
                    print('Validation cost decreased ({:.6f} --> {:.6f}).'.format(
                        self.resources['valid_cost_min'],
                        valid_cost))

                    # store the best state dictionaries
                    self.resources['best_model_state_dict'] = self.resources['model'].state_dict()
                    self.resources['best_epoch'] = epoch

                    # update minimum validation cost
                    self.resources['valid_cost_min'] = valid_cost

                # print validation accuracy
                print('Validation Accuracy: %2d%% (%2d/%2d)' % (
                    100. * np.sum(class_correct) / np.sum(class_total),
                    np.sum(class_correct), np.sum(class_total)))


            # save the current epoch
            self.resources['current_epoch'] = epoch

            # save the checkpoint
            self.save()

        # show cost graphic
        plt.plot(self.resources['train_cost'], color='r', linewidth=2, label='Training Cost')
        plt.plot(self.resources['valid_cost'], color='b', linewidth=2, label='Validation Cost')
        plt.xlabel('Epochs')
        plt.ylabel('Cost')
        plt.title('Training Costs')
        plt.legend()
        plt.show()


    def test(self):
        # move model to GPU is CUDA if available
        if torch.cuda.is_available():
            self.resources['model'].cuda()

        # validate the model
        self.resources['model'].eval()

        class_correct = list(0. for i in range(len(self.resources['test_dataloader'].dataset.classes)))
        class_total = list(0. for i in range(len(self.resources['test_dataloader'].dataset.classes)))

        # track test cost
        test_cost = 0.0

        with torch.no_grad():
            # iterate over test data
            for batch_idx, (data, target) in enumerate(self.resources['test_dataloader']):

                # move tensors to GPU if CUDA is available
                if torch.cuda.is_available():
                    data, target = data.cuda(), target.cuda()

                # forward pass
                output = self.resources['model'](data)

                # calculate the batch cost
                cost = self.resources['criterion'](output, target)

                # update test cost
                test_cost += cost.item() * data.size(0)

                # convert output probabilities to predicted class
                _, pred = torch.max(output, 1)

                # compare predictions to true label
                correct_tensor = pred.eq(target.data.view_as(pred))

                # use appropriate dimensions
                if torch.cuda.is_available():
                    correct = np.squeeze(correct_tensor.cpu().numpy())
                else:
                    correct = np.squeeze(correct_tensor.numpy())

                # calculate test accuracy for each object class
                for i in range(len(data.data)):
                    label = target.data[i]-1
                    class_correct[label] += correct[i].item()
                    class_total[label] += 1

        # calculate average test cost
        test_cost = test_cost / len(self.resources['test_dataloader'].dataset)

        # print total test cost
        print('Test cost: {:.6f}\n'.format(test_cost))

        # print test accuracy for each class
        for i in range(len(self.resources['test_dataloader'].dataset.classes)):
            if class_total[i] > 0:
                print('Test Accuracy of %5s: %2d%% (%2d/%2d)' % (
                    self.resources['cat_to_name'][str(i + 1)], 100 * class_correct[i] / class_total[i],
                    np.sum(class_correct[i]), np.sum(class_total[i])))
            else:
                print('Test Accuracy of %5s: N/A (no training examples)' % (self.resources['cat_to_name'][str(i + 1)]))

        # print total test accuracy
        print('\nTest Accuracy (Overall): %2d%% (%2d/%2d)' % (
            100. * np.sum(class_correct) / np.sum(class_total),
            np.sum(class_correct), np.sum(class_total)))

    # 'class_to_idx': self.resources['class_to_idx'],
    def save(self, file_path='model.pth'):
        checkpoint = {
            'current_epoch': self.resources['current_epoch'],
            'best_epoch': self.resources['best_epoch'],
            'train_cost': self.resources['train_cost'],
            'valid_cost': self.resources['valid_cost'],
            'valid_cost_min': self.resources['valid_cost_min'],
            'optim_state_dict': self.resources['optimizer'].state_dict(),
            'best_model_state_dict': self.resources['best_model_state_dict'],
            'latest_model_state_dict': self.resources['model'].state_dict(),
            'valid_idx': self.resources['valid_idx'],
            'test_idx': self.resources['test_idx'],
            'train_idx': self.resources['train_idx'],
            'class_to_idx': self.resources['class_to_idx'],
            'idx_to_class': self.resources['idx_to_class']
        }
        torch.save(checkpoint, file_path)

    def print_checkpoint(self):
        print('Number of Epochs:', self.resources['current_epoch'])
        print('Best Epoch:', self.resources['best_epoch'])
        print('Minimum Validation Cost:', self.resources['valid_cost_min'])
        # show cost graphic
        plt.plot(self.resources['train_cost'], color='r', linewidth=2, label='Training Cost')
        plt.plot(self.resources['valid_cost'], color='b', linewidth=2, label='Validation Cost')
        plt.xlabel('Epochs')
        plt.ylabel('Cost')
        plt.title('Training Costs')
        plt.legend()
        plt.show()


    def load_checkpoint(self, checkpoint_path):
        # load a pre-trained model
        self.resources['model'] = models.resnet50(pretrained=True)

        # load the checkpoint
        if torch.cuda.is_available():
            checkpoint = torch.load(checkpoint_path)
        else:
            checkpoint = torch.load(checkpoint_path, map_location=torch.device('cpu'))

        # create new feedforward layers
        classifier = nn.Sequential(OrderedDict([
            ('fc1', nn.Linear(2048, 1000)),
            ('relu', nn.ReLU()),
            ('dropout', nn.Dropout(0.6)),
            ('fc2', nn.Linear(1000, 102)),
            ('softmax', nn.LogSoftmax(dim=1))
        ]))
        self.resources['model'].fc = classifier

        # load best model state dictionary
        self.resources['best_model_state_dict'] = checkpoint['best_model_state_dict']

        # load latest model
        self.resources['latest_model_state_dict'] = checkpoint['latest_model_state_dict']
        self.resources['model'].load_state_dict(checkpoint['latest_model_state_dict'])

        # specify cost function
        self.resources['criterion'] = nn.NLLLoss()

        # specify optimizer
        self.resources['optimizer'] = optim.SGD(self.resources['model'].parameters(), lr=0.01)

        # specify learning rate decay
        self.scheduler = lr_scheduler.StepLR(self.resources['optimizer'], step_size=9, gamma=0.9)

        # load the current epoch
        self.resources['current_epoch'] = checkpoint['current_epoch']+1

        # load the best epoch
        self.resources['best_epoch'] = checkpoint['best_epoch']+1

        # load the cost array
        self.resources['train_cost'] = checkpoint['train_cost']
        self.resources['valid_cost'] = checkpoint['valid_cost']

        # load the minimum validation cost
        self.resources['valid_cost_min'] = checkpoint['valid_cost_min']

        # load the dataset distribution (train/valid/test)
        self.resources['valid_idx'] = checkpoint['valid_idx']
        self.resources['test_idx'] = checkpoint['test_idx']
        self.resources['train_idx'] = checkpoint['train_idx']

        # load the class value - id mapping
        self.resources['class_to_idx']=checkpoint['class_to_idx']
        self.resources['idx_to_class']=checkpoint['idx_to_class']

        # load optimizer state dictionary
        self.resources['optimizer'].load_state_dict(checkpoint['optim_state_dict'])




    def predict(self, image_path, topk=5):

        if not os.path.isfile(image_path):
            print('Wrong image path!')
            sys.exit(2)

        # load an image
        image = process_image(image_path)

        # load the best model
        self.resources['model'].load_state_dict(self.resources['best_model_state_dict'])

        # evaluate the model version
        self.resources['model'].eval()

        # move model to GPU is CUDA if available
        if torch.cuda.is_available():
            image.cuda()

        # load back the latest model version
        self.resources['model'].load_state_dict(self.resources['latest_model_state_dict'])

        # forward the image through the network
        output = self.resources['model'](image)

        # apply exp() to the output and get the probabilities
        probs = torch.exp(output)

        # select top-k probabilities
        top_probs, top_labels = probs.topk(topk)

        # reverse the order of the top-k probabilities and labels for plotting purposes
        top_probs = list(reversed(torch.squeeze(top_probs).detach().numpy().tolist()))
        top_idx = list(reversed(torch.squeeze(top_labels).detach().numpy().tolist()))

        # convert idx to classes
        top_labels = [self.resources['idx_to_class'][idx] for idx in top_idx]

        # transform classes to flower names
        top_flowers = [self.resources['cat_to_name'][str(lab)] for lab in top_labels]

        fig, ax = plt.subplots(ncols=2, figsize=(10, 4))
        imshow(image, ax[1], top_flowers[len(top_flowers)-1])

        ax[0].barh(top_flowers, top_probs)
        plt.show()

def main(argv):

    model = Model(batch_size=64)

    try:
        opts, args = getopt.getopt(argv,'t:p:ncah',['train=', 'predict=', 'new', 'checkpoint', 'analyse', 'help'])
    except getopt.GetoptError:
        # exception run when unidentified argument is passed
        sys.exit('\n\nInvalid command line!\n\n-t,\t--train=VALUE\t\ttrains the model for a specified number of epochs\n-p,\t--predict=PATH\t'
              '\tpredicts the class for an image file at the specified path\n-n,\t--new\t\t\tcreates a new model\n-c,'
              '\t--checkpoint\t\tshows model performance evolution\n-a,\t--analyse'
              '\t\tcalculates model accuracy over the test set\n')
    options = {}
    for opt, arg in opts:
        options[opt] = arg

    # if 'help' command or no command
    if '-h' in options.keys() or '--help' in options.keys() or len(options.keys())==0:
        print('\nHELP\n\n-t,\t--train=VALUE\t\ttrains the model for a specified number of epochs\n-p,\t--predict=PATH\t'
              '\tpredicts the class for an image file at the specified path\n-n,\t--new\t\t\tcreates a new model\n-c,'
              '\t--checkpoint\t\tshows model performance evolution\n-a,\t--analyse'
              '\t\tcalculates model accuracy over the test set\n')

    ### Loading dataset and/or checkpoint ###

    # if 'train' command
    if '-t' in options.keys() or '--train' in options.keys():
        # if 'new' command or if checkpoint missing
        if '-n' in options.keys() or '--new' in options.keys() or not os.path.isfile('model.pth'):
            # load checkpoint
            model.load('dataset/images', 'dataset/annotations/imagelabels.mat', True)
            model.save()
        else:
            # load checkpoint
            model.load_checkpoint('model.pth')
            model.load('dataset/images', 'dataset/annotations/imagelabels.mat', False)

        if '-t' in options.keys():
            no_of_epochs = int(options['-t'])
        else:
            no_of_epochs = int(options['--train'])

        model.train(no_of_epochs)

    # if 'analyse' command
    elif '-a' in options.keys() or '--analyse' in options.keys():
        if os.path.isfile('model.pth'):
            model.load_checkpoint('model.pth')
            model.load('dataset/images', 'dataset/annotations/imagelabels.mat', False)
        else:
            print("'model.pth' file is missing, a new one will be generated!")
            model.load('dataset/images', 'dataset/annotations/imagelabels.mat', True)
            model.save()
        model.test()

    # if 'checkpoint' command
    elif '-c' in options.keys() or '--checkpoint' in options.keys():
        if os.path.isfile('model.pth'):
            model.load_checkpoint('model.pth')
            model.print_checkpoint()
        else:
            print("No checkpoint is available, 'model.pth' file is missing!")

    elif '-p' in options.keys() or '--predict' in options.keys():
        if os.path.isfile('model.pth'):
            model.load_checkpoint('model.pth')
            if '-p' in options.keys():
                image_path = str(options['-p'])
            else:
                image_path = str(options['--predict'])

            model.predict(image_path)
        else:
            print("No checkpoint is available, 'model.pth' file is missing! Prediction is impossible.")



if __name__ == '__main__':
    # run the terminal command
    main(sys.argv[1:])

