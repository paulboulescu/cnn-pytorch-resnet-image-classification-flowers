# CNN (ResNet) Flowers Image Classifier with PyTorch

## About
Convolutional Neural Network model with PyTorch, based on a pre-trained ResNet-34 architecture, used to classify images of flowers from 102 species. 

## Instalation
* **Clone the repository:**
```
$ git clone https://paulboulescu@bitbucket.org/paulboulescu/cnn-pytorch-resnet-image-classification-flowers.git
```

* **Change the directory:**
```
$ cd cnn-pytorch-resnet-image-classification-flowers
```

* **Optional, for training and accuracy testing:**
	* Download and unpack the [Flower Dataset](http://www.robots.ox.ac.uk/~vgg/data/flowers/102/102flowers.tgz) into the folder *dataset/images/*
	* Download the [image labels](http://www.robots.ox.ac.uk/~vgg/data/flowers/102/imagelabels.mat) and save it into the folder *dataset/annotations/*

## Commands
* **Predict** - Will predict top-five classes for an image.
```
$ python model.py -p <image path>
```

* **Test Accuracy** - Will calculate the model's accuracy over the test set.
```
$ python model.py -a
```

* **Show Checkpoint** - Will display details about the model.
```
$ python model.py -c
```

* **Continue Training** - Will train the model from its latest checkpoint and save the evolution.
```
$ python model.py -t <number of epochs>
```

* **Help** - Will display details of command options.
```
$ python model.py -h
```

## Requirements
1. PyTorch 1.0
2. NumPy
3. SciPy
4. Matplotlib

## Details
The model was pre-trained. The dataset used for training is [102 Category Flower Dataset](http://www.robots.ox.ac.uk/~vgg/data/flowers/102/), from Visual Geometry Group - Deparment of Engineering Science, University of Oxford. It consists of 8189 images of flowers from 102 categories. Each class has between 40 and 258 images. The dataset was split in training (60%), validation (20%), and testing (20%). After 70 epochs, the minimum validation lost was reached in the 69th epoch: 0.08349. The test accuracy is **98.2%** (1574/1602).

![Training Costs](assets/trainingcosts.png)

## Prediction
The model predicts probabilities for top-five classes.

![Prediction](assets/prediction.png)

## Base Network
ResNet-50 - the original classifier layers were replaced by two fully connected layers.

## Model Details
* Classification Layers: **[In:2048, FC: 1000, Out:102]**
* Activation: **ReLu / Softmax**
* Dropout: **0.6**
* Learning Rate: **0.03**
* Rate Decay:
	* Step: **9**
	* Gamma: **0.9**
* Epochs: **60**
* MiniBatch: **64**
* Optimizer: **Gradient Descent**
* Loss: **Negative Log Likelihood Loss**
* Data Augmentation:
	* Random Cropping: **60% - 100%**
	* Random Flip: **Horizontal**
	* Random Rotation: **45 degrees**

## Internal Dependencies
* *model.py* - the model, options to save/load/test, command line interface
* *utils.py* - data processing and visualisation
* *model.pth* - checkpoint of the model, saved each time the validation loss reaches a new minimum
* *cat_to_name.json* - maps the class to the name of the flower
* *dataset/annotations/imagelabels.mat* - classification of the images in the dataset
* *dataset/images/** - images used for training/validation/classification

## Disclaimer
Created under the PyTorch Scholarship Challenge ([Udacity](https://www.udacity.com)). The code uses ideas, datasets, algorithms, and code fragments presented in the Course. The architecture is based on the [ResNet](https://arxiv.org/abs/1512.03385) model.