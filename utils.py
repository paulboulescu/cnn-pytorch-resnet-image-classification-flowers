
import numpy as np
import matplotlib.pyplot as plt
import torch
from PIL import Image
from torch.utils.data import Dataset
import scipy.io
import os


def imshow(image, ax=None, title=None):
    if ax is None:
        fig, ax = plt.subplots()

    image = torch.squeeze(image)

    # move channel dimension last
    image = image.numpy().transpose((1, 2, 0))

    # target mean and standard deviation
    mean = np.array([0.485, 0.456, 0.406])
    std = np.array([0.229, 0.224, 0.225])

    # restore image to initial file mean and standard deviation
    image = std * image + mean

    # clip the image to values between 0 and 1
    image = np.clip(image, 0, 1)

    # remove tick labels
    ax.set_yticklabels([])
    ax.set_xticklabels([])

    # show the image
    ax.imshow(image)

    # set the image title
    ax.set_title(title)

    return ax

def process_image(image):
    # image target size
    final_size = 224

    # target mean and standard deviation
    mean = np.array([0.485, 0.456, 0.406])
    std = np.array([0.229, 0.224, 0.225])

    # open the image file
    pil_image = Image.open(image)

    # get image dimensions
    width, height = pil_image.size

    # determine the image minimum dimension
    size = min(width, height)

    # crop the image to a rectangle
    pil_image = pil_image.crop(((width - size) / 2, (height - size) / 2, (width + size) / 2, (height + size) / 2))

    # resize the image to final size
    pil_image.thumbnail((final_size, final_size))

    # convert image to numpy array and apply target mean and standard deviation
    np_image = np.array(pil_image, dtype=np.float32) / 255
    np_image = (np_image - mean) / std

    # move channel dimension first
    np_image = np.transpose(np_image, (2, 0, 1))

    # create pytorch tensor of the image
    image = torch.from_numpy(np_image).type(torch.FloatTensor)

    # reduce tensor dimensions
    image = image.unsqueeze(0)

    return image



def splitData(labels, valid_size=0.2, test_size=0.2, ):

    # get a list of all the indices
    indices = list(range(len(labels)))

    # create the arrays which will store the final splited indices
    train = np.array([], dtype=np.int)
    valid = np.array([], dtype=np.int)
    test = np.array([], dtype=np.int)

    # loop over each class
    for class_val in set(labels):

        # select all indices that belong to that class
        selected_idx = [index_val for index_val in indices if labels[index_val] == class_val]

        # shuffle them
        np.random.shuffle(selected_idx)

        # determine the length of each split
        valid_split = int(np.floor(valid_size * len(selected_idx)))
        test_split = int(np.floor(test_size * len(selected_idx)))

        # extract the indices in the calculated distribution
        valid_idx, test_idx, train_idx = selected_idx[:valid_split], selected_idx[valid_split:valid_split + test_split], selected_idx[valid_split + test_split:]

        # append to the final array
        train = np.append(train, train_idx)
        valid = np.append(valid, valid_idx)
        test = np.append(test, test_idx)

    return train, valid, test


class CustomDataset(Dataset):

    def __init__(self, files, labels, transform=None):
        """
        Args:
            mat_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.labels = labels
        self.files = files
        self.transform = transform

    def __len__(self):
        return len(self.files)

    def pil_loader(self, path):
        pil_image = Image.open(path)

        return pil_image

    @property
    def classes(self):
        classes = set(self.labels)
        return classes

    def __getitem__(self, idx):

        sample = self.pil_loader(self.files[idx])

        if self.transform:
            sample = self.transform(sample)

        return sample, torch.tensor(self.labels[idx], dtype=torch.long)